var signIn = new OktaSignIn({
    baseUrl: 'https://metrolinx.oktapreview.com',
    // helpLinks: {
    //     help: 'https://metrolinx.oktapreview.com/help/login'
    // },
    language: 'fr',
    i18n: {
        en: {
            'primaryauth.submit': 'Log In',
            'primaryauth.username.placeholder': 'Your Metrolinx ID',
        },
        fr: {
          'primaryauth.submit': 'Se connecter',
          'primaryauth.username.placeholder': "Votre nom d'utilisateur Metrolinx",
        }
    }
});

signIn.renderEl({
  el: '#widget-container'
}, function success(res) {
  if (res.status === 'SUCCESS') {
    console.log('Do something with this sessionToken', res.session.token);
    res.session.setCookieAndRedirect('https://metrolinx.oktapreview.com/app/UserHome');
  } else {
  // The user can be in another authentication state that requires further action.
  // For more information about these states, see:
  //   https://github.com/okta/okta-signin-widget#rendereloptions-success-error
  }
});