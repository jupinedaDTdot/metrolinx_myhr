function onTabClick(e, id) {
    const tab = e.target;
    
    if(tab.className === "tab-nonactive") {
        const activeTab = document.getElementsByClassName("tab-active")[0];
        activeTab.className = activeTab.className.replace("tab-active", "tab-nonactive");
        tab.className = tab.className.replace("tab-nonactive", "tab-active");

        const tabContent = document.getElementsByClassName("tab-content");
        for (let i=0; i< tabContent.length; i++) {
            tabContent[i].style.display = "none";
        }

        const showContent = document.getElementById(id);
        showContent.style.display = "block";
    }

}