/* File: gulpfile.js */
'use strict'
// grab our gulp packages
var gulp 		= require('gulp'),
    changed 	= require('gulp-changed'),
    runSequence = require('run-sequence'),
    newer 		= require('gulp-newer'),
    browserSync = require('browser-sync').create(),
    sass        = require('gulp-sass'),
    sourcemaps  = require('gulp-sourcemaps'),
    cssmin      = require('gulp-cssmin'),
    autoprefixer = require('gulp-autoprefixer'),
    del         = require('del'),
    pug         = require('gulp-pug');

var autoprefixerOptions = {
    browsers: ['last 5 Chrome versions', 'last 5 Firefox versions', 'ie >= 8']
};

gulp.task('browserSync', function () {
    browserSync.init(['./build/**.*'], {
        port: 3000,
        server: {
            baseDir: './',
            directory: true,
            browser: 'google chrome',
            logLevel: 'debug'
        },
        files: ['build/**/*.{html,js,css}'],
        socket: {
            heartbeatTimeout: 6000
        }
        // reloadDebounce: 15000
    });
});
// run this task by typing in gulp pug in CLI
gulp.task('pug', function () {
    return gulp.src('src/pug/pages/*.pug')
      .pipe(pug()) // pipe to pug plugin
      .pipe(gulp.dest('build')); // tell gulp our output folder
});
gulp.task('html', function (doneTemplates) {
    return gulp.src('./src/pug/pages/*.pug')
        .pipe(newer('build'))
        .pipe(pug({
            locals: './src/pug/',
            pretty: true
        }))
        .pipe(gulp.dest('build'));
});
gulp.task('css', function () {
    return gulp.src('./src/scss/*.scss')
        // Write the maps only for development
        // .pipe(changed('./build/css', { extension: '.css' }))
        // .pipe(sourcemaps.init())
        .pipe(sass({
            outputStyle: 'expanded',
            omitSourceMapUrl: true,
            precision: 2,
            indentType: 'tab',
            indentWidth: 1
        })
        .on('error', sass.logError))
        // Write the maps only for development
        // .pipe(autoprefixer(autoprefixerOptions))
        // .pipe(sourcemaps.write('.'))
        .pipe(gulp.dest('./build/css'))
        .pipe(browserSync.reload({ stream: true }));
});
gulp.task('page-css', function () {
    return gulp.src('./src/scss/pages/*.scss')
        // Write the maps only for development
        // .pipe(changed('./build/css', { extension: '.css' }))
        // .pipe(sourcemaps.init())
        .pipe(sass({
            outputStyle: 'expanded',
            omitSourceMapUrl: true,
            precision: 2,
            indentType: 'tab',
            indentWidth: 1
        })
        .on('error', sass.logError))
        // Write the maps only for development
        // .pipe(autoprefixer(autoprefixerOptions))
        // .pipe(sourcemaps.write('.'))
        .pipe(gulp.dest('./build/css/pages'))
        .pipe(browserSync.reload({ stream: true }));
});
gulp.task('scripts', function () {
    return gulp.src('./src/javascript/*.js')
        .pipe(gulp.dest('./build/javascript'))
        .pipe(browserSync.reload({ stream: true }));
});
gulp.task('fonts', function () {
    return gulp.src('./src/fonts/*')
        .pipe(gulp.dest('./build/fonts'))
        .pipe(browserSync.reload({ stream: true }));
});
gulp.task('assets', function () {
    return gulp.src('./src/assets/*')
        .pipe(gulp.dest('./build/assets'))
        .pipe(browserSync.reload({ stream: true }));
});
// Clean dev folder and prod folder
gulp.task('clean', function () {
    return del(['./build/']);
});
// Build new dev folder and prod folder
gulp.task('build', function (cb) {
    return runSequence(
        'clean',
        ['assets', 'fonts', 'scripts', 'css', 'page-css', 'html'],
        cb
    );
});
gulp.task('watch', function () {
    gulp.watch('./src/pug/**/*.pug', ['html', browserSync.reload]);
    gulp.watch('./src/scss/**/*.scss', ['css', browserSync.reload]);
    gulp.watch('./src/scss/pages/**/*.scss', ['page-css', browserSync.reload]);
    gulp.watch('./src/javascript/**/*.js', ['scripts', browserSync.reload]);
    gulp.watch('./src/fonts/**/*', ['fonts', browserSync.reload]);
    gulp.watch('./src/assets/**/*', ['fonts', browserSync.reload]);

});

gulp.task('default', ['browserSync', 'watch']);