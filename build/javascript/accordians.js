function accToggle(e) {
    const chevron = e.currentTarget;
    const content = e.currentTarget.parentElement.getElementsByClassName('content-acc')[0];

    if (window.getComputedStyle(content).getPropertyValue('display') === 'none') {
        content.style.display = 'block';
        chevron.style.transform = 'rotate(180deg)';
    } else {
        content.style.display = 'none'
        chevron.style.transform = null;
    }
}