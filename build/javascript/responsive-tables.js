function displayTable() {
    const tables = document.getElementsByClassName('responsive-table');

    for (let i=0; i<tables.length; i++) {
        const labels = [];
        const headers = tables[i].getElementsByTagName('thead')[0].getElementsByTagName('th');

        for (let j=0; j<headers.length; j++) {
            labels.push(headers[j].innerText);
        }

        const rows = tables[i].getElementsByTagName('tbody')[0].getElementsByTagName('tr');
        
        for (let j=0; j<rows.length; j++) {
            const row = rows[j].getElementsByTagName('td');
            for (let k=0; k<row.length; k++) {
                row[k].setAttribute("data-label", labels[k]);
            }
        }
    
    }
}

displayTable();