var carousels = document.getElementsByClassName('image-carousel-container')
var slider_arr = [];

for (let i=0; i<carousels.length; i++) {

    slider_arr.push(tns({
        container: `.image-carousel${i}`,
        items: 4,
        slideBy: 'page',
        gutter: '30',
        controls: false,
        nav: false
      }));

    carousels[i].querySelector('.right-arrow').onclick = function () {
        slider_arr[i].goTo('next');
    };

    carousels[i].querySelector('.left-arrow').onclick = function () {
        slider_arr[i].goTo('prev');
    };
}