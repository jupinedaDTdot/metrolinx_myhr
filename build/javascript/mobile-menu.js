function toggleMenu() {
    const menuBtn = document.getElementsByClassName('mobile-menu-btn')[0];
    const closeBtn = document.getElementsByClassName('mobile-close-btn')[0];

    const nav = document.getElementsByClassName('item-2')[0];
    if (nav.classList.contains('show')) {

        nav.classList.remove('show');
        nav.classList.add('hide');
        document.body.style.overflowY = "scroll";

        menuBtn.classList.remove('hide');
        closeBtn.classList.remove('reveal');

    } else {

        nav.classList.remove('hide');
        nav.classList.add('show');
        document.body.style.overflowY = "hidden";

        menuBtn.classList.add('hide');
        closeBtn.classList.add('reveal');

    }
}